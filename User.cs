﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class User
    {
        public string nick { get; set; }
        public string username { get; set; }
        public uint id { get; set; }
        public string password_hash { get; set; }
        public bool connected { get; set; }
    }
}
