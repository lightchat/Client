﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public enum NetMessageType
    {
        ChannelMessage,
        DirectMessage,
        JoinChannel,
        LeaveChannel,
        ChangeName,
        ConfirmNickChange,
        ConfirmChannelJoin,
        ConfirmChannelLeave,
        RequestChannelUsers,
        SendChannelUsers,
        RequestChannelMOTD,
        SendChannelMOTD,
        RequestWHOIS,
        SendWHOIS
    }

    public enum MessageType
    {
        UserChannelMessage,
        UserDirectMessage,
        ConnectMessage,
        DisconnectMessage,
        ChannelConnectMessage,
        ChannelLeaveMessage,
        StatusMessage,
        ChangeName,
        Command
    }

    public enum StatusMessage
    {
        ErrChannelPassword,
        ErrCannotLeave
    }

    public partial class MainWindow : Window
    {
        public User user = new User();
        public NetClient client;
        public List<Channel> channels = new List<Channel>();
        public List<TabItem> tab_channels = new List<TabItem>();
        public List<RichTextBox> rtext_boxes = new List<RichTextBox>();
        public List<TextBox> motd_text_boxes = new List<TextBox>();
        public List<ListBox> users_lists = new List<ListBox>();
        


        public string selected_channel { get; set; }

        public void SendJoinChannel(string channel)
        {
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)NetMessageType.JoinChannel);
            outmsg.Write(channel);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
            client.FlushSendQueue();
        }

        public void SendLeaveChannel(string channel)
        {
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)NetMessageType.LeaveChannel);
            outmsg.Write(channel);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
            client.FlushSendQueue();
        }

        public void SendChannelMessage(string channel, string message)
        {
            if(!user.connected)
            {
                AppendText("You are not connected.", selected_channel);
                return;
            }
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)NetMessageType.ChannelMessage);
            outmsg.Write(channel);
            outmsg.Write(message);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
            client.FlushSendQueue();
        }

        public void SendNickChange(string nick)
        {
            if (!user.connected)
            {
                AppendText("You are not connected.", selected_channel);
                return;
            }
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)NetMessageType.ChangeName);
            outmsg.Write(nick);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
            client.FlushSendQueue();
        }

        public void RequestMOTD(string channel)
        {
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)NetMessageType.RequestChannelMOTD);
            outmsg.Write(channel);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
            client.FlushSendQueue();
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            enter_text_box.Focusable = true;
            Keyboard.Focus(enter_text_box);

            //AppendText("Type /help to see commands.", selected_channel , "#226de5");
            //AppendText("Use the /join <channel> command to join a channel.", selected_channel , "#074ab7");

            NetPeerConfiguration config = new NetPeerConfiguration("LightChat");

            config.EnableMessageType(NetIncomingMessageType.Data);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.EnableMessageType(NetIncomingMessageType.Error);
            config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.WarningMessage);

            config.AutoFlushSendQueue = false;
            client = new NetClient(config);
            client.Start();
            user.connected = false;

            client.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
            client.Connect("90.164.90.25", 21002);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            client.Shutdown("bye");
        }

        public string getColor(string hexcolor)
        {
            return ((Color)ColorConverter.ConvertFromString(hexcolor)).ToString();
        }

        public string HashPassword(string raw_password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(raw_password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string savedPasswordHash = Convert.ToBase64String(hashBytes);

            return savedPasswordHash;
        }

        public void CommandParser(string raw_msg)
        {
            string msg = raw_msg.Replace("/", "");
            string command = msg.Split(' ').First();
            string[] args = msg.Split(' ').Skip(1).ToArray();

            switch(command)
            {
                case "help":
                    AppendText("Commands: register, login, help, join, leave, nickname, status", selected_channel , "#226de5");
                    break;
                case "register":
                    if (args.Length < 2)
                    {
                        AppendText("Usage: /login <username> <password>", selected_channel , "#226de5");
                        return;
                    }
                    user.username = args[0];
                    user.password_hash = HashPassword(args[1]);

                    // send server login..

                    AppendText("Register: Not developed yet!", selected_channel, "#dd3623");
                    break;
                case "login":
                    if(args.Length < 2)
                    {
                        AppendText("Usage: /login <username> <password>", selected_channel, "#226de5");
                        return;
                    }
                    AppendText("Login: Not developed yet!", selected_channel, "#dd3623");
                    break;
                case "join":
                    if(args.Length > 0)
                        SendJoinChannel(args[0]);
                    else
                        AppendText("Usage: /join <channel>", selected_channel, "#226de5");
                    break;
                case "leave":
                    if(args.Length > 0)
                    {
                        SendLeaveChannel(args[0]);
                    }
                    else
                    {
                        AppendText("Usage: /leave <channel>", selected_channel, "#226de5");
                    }
                    
                    break;
                case "nickname":
                    if(args.Length < 1)
                    {
                        AppendText("Usage: /nickname <name>", selected_channel, "#226de5");
                        return;
                    }

                    SendNickChange(args[0]);
                    break;
                case "status":
                    AppendText("Status: " + client.Status.ToString(), selected_channel, "#dd3623");
                    break;
            }
        }

        private void enter_text_box_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                if (String.IsNullOrWhiteSpace(enter_text_box.Text))
                    return;


                if (enter_text_box.Text.StartsWith("/"))
                {
                    CommandParser(enter_text_box.Text);
                    enter_text_box.Text = "";
                    return;
                }

                send_text(enter_text_box.Text);
                enter_text_box.Text = "";
            }
        }

        private void send_text(string Text)
        {
            if(client.Status == NetPeerStatus.NotRunning || client.ConnectionStatus != NetConnectionStatus.Connected)
            {
                AppendText("You are not connected!", selected_channel, getColor("#dd3623"));
                return;
            }

            if(String.IsNullOrWhiteSpace(user.nick))
            {
                AppendText("You need to set a nickname! Either with (/nickname <name>) or via Settings/Preferences.", selected_channel, "#dd3623");
                return;
            }

            // Send text to server...
            var sel_channel_name = (String)((TabItem)tab_control.SelectedItem).Header;
            SendChannelMessage(sel_channel_name, Text);
        }

        private static readonly Regex UrlRegex = new Regex(@"(?#Protocol)(?:(?:ht|f)tp(?:s?)\:\/\/|~/|/)?(?#Username:Password)(?:\w+:\w+@)?(?#Subdomains)(?:(?:[-\w]+\.)+(?#TopLevel Domains)(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel|[a-z]{2}))(?#Port)(?::[\d]{1,5})?(?#Directories)(?:(?:(?:/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|/)+|\?|#)?(?#Query)(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&amp;(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?#Anchor)(?:#(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)?");

        public static bool IsHyperlink(string word)
        {
            // First check to make sure the word has at least one of the characters we need to make a hyperlink
            if (word.IndexOfAny(@":.\/".ToCharArray()) != -1)
            {
                if (Uri.IsWellFormedUriString(word, UriKind.Absolute))
                {
                    // The string is an Absolute URI
                    return true;
                }
                else if (UrlRegex.IsMatch(word))
                {
                    Uri uri = new Uri(word, UriKind.RelativeOrAbsolute);

                    if (!uri.IsAbsoluteUri)
                    {
                        // rebuild it it with http to turn it into an Absolute URI
                        uri = new Uri(@"http://" + word, UriKind.Absolute);
                    }

                    if (uri.IsAbsoluteUri)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void link_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri.ToString());
        }

        private void GotMessage(object state)
        {
            NetIncomingMessage msg;
            while ((msg = client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        {
                            AppendText("Error/warning/debug/verbose: " + msg.ReadString(), selected_channel);
                            break;
                        }                        
                    case NetIncomingMessageType.StatusChanged:
                        {
                            var status_byte = msg.ReadByte();
                            var status = (NetConnectionStatus)status_byte;

                            switch (status)
                            {
                                case NetConnectionStatus.Connected:
                                    {
                                        user.connected = true;

                                        if (String.IsNullOrWhiteSpace(Properties.Settings.Default.Nick))
                                        {
                                            var EnterNick = new EnterNickWindow();
                                            EnterNick.ShowDialog();
                                        }

                                        SendNickChange(Properties.Settings.Default.Nick);
                                        break;
                                    }
                            }
                            break;
                        }                        
                    case NetIncomingMessageType.Data:
                        {
                            var type = (NetMessageType)msg.ReadByte();
                            switch (type)
                            {
                                case NetMessageType.RequestChannelMOTD:
                                    {
                                        var channel_name = msg.ReadString();
                                        var motd = msg.ReadString();

                                        if (!String.IsNullOrWhiteSpace(motd))
                                        {
                                            AppendText("Channel MOTD changed to: " + motd, selected_channel, "#3ec9af");
                                        }                                        
                                        motd_text_boxes.Find(x => x.Name == channel_name + "_motd").Text = motd;
                                        break;
                                    }
                                case NetMessageType.ConfirmNickChange:
                                    {
                                        var nick = msg.ReadString();
                                        user.nick = nick;
                                        Properties.Settings.Default.Nick = user.nick;
                                        Properties.Settings.Default.Save();
                                        Username_Label.Content = user.nick;
                                        break;
                                    }
                                case NetMessageType.ChannelMessage:
                                    {
                                        var msg_type = (MessageType)msg.ReadByte();
                                        string channel = "";

                                        switch (msg_type)
                                        {
                                            case MessageType.UserChannelMessage:
                                                {
                                                    var channel_name = msg.ReadString();
                                                    var user_nick = msg.ReadString();
                                                    var message = msg.ReadString();
                                                    var color = msg.ReadString();

                                                    channel = channel_name;

                                                    AppendText("<" + user_nick + ">: " + message, channel_name);
                                                    break;
                                                }
                                            case MessageType.StatusMessage:
                                                {
                                                    var status_msg_type = (StatusMessage)msg.ReadByte();

                                                    switch (status_msg_type)
                                                    {
                                                        case StatusMessage.ErrCannotLeave:
                                                            {
                                                                AppendText("You can't leave this channel", "Status", "#f25226");
                                                                break;
                                                            }
                                                        case StatusMessage.ErrChannelPassword:
                                                            {
                                                                // Finish when channel support is added.
                                                                break;
                                                            }
                                                    }
                                                    break;
                                                }
                                            case MessageType.DisconnectMessage:
                                                {
                                                    var channel_name = msg.ReadString();
                                                    var user_nick = msg.ReadString();

                                                    channel = channel_name;

                                                    AppendText(user_nick + " disconnected.", channel_name, "#ff4a32");
                                                    break;
                                                }
                                            case MessageType.ChannelConnectMessage:
                                                {
                                                    var channel_name = msg.ReadString();
                                                    var user_nick = msg.ReadString();

                                                    channel = channel_name;

                                                    AppendText(user_nick + " joined the channel.", channel_name, "#267ef2");
                                                    break;
                                                }
                                            case MessageType.ChannelLeaveMessage:
                                                {
                                                    var channel_name = msg.ReadString();
                                                    var nick = msg.ReadString();

                                                    channel = channel_name;

                                                    AppendText(nick + " left the channel.", channel_name, "#267ef2");
                                                    break;
                                                }
                                            case MessageType.ChangeName:
                                                {
                                                    var channel_name = msg.ReadString();
                                                    var last_nick = msg.ReadString();
                                                    var nick = msg.ReadString();

                                                    AppendText(last_nick + " has changed his nickname to: " + nick, channel_name, "#ffba3a");
                                                    break;
                                                }
                                        }

                                        if (!String.IsNullOrWhiteSpace(channel))
                                        {
                                            if ((String)((TabItem)tab_control.SelectedItem).Header != channel)
                                            {
                                                var bc = new BrushConverter();
                                                tab_channels.Find(x => (String)x.Header == channel).Background = (Brush)bc.ConvertFrom("#c8daf7");
                                            }
                                        }                                                                               
                                        break;
                                    }
                                case NetMessageType.DirectMessage:
                                    {
                                        // finish this
                                        var username = msg.ReadString();
                                        var message = msg.ReadString();
                                        break;
                                    }
                                case NetMessageType.ChangeName:
                                    {
                                        break;
                                    }
                                case NetMessageType.ConfirmChannelJoin:
                                    {
                                        var joined = msg.ReadBoolean();
                                        var channel = msg.ReadString();
                                        var motd_msg = msg.ReadString();

                                        if (joined)
                                        {
                                            TabItem item = null;
                                            Grid grid = null;
                                            RichTextBox textbox = null;
                                            ListBox listbox = null;
                                            TextBox motd_box = null;
                                            FlowDocument doc = null;
                                            ContextMenu cmenu = null;
                                            MenuItem CMenuItem = null;

                                            try
                                            {
                                                textbox = new RichTextBox();
                                                doc = new FlowDocument();

                                                textbox.Margin = new Thickness(0, 25, 250, 0);
                                                textbox.IsReadOnly = true;
                                                textbox.Name = channel + "_chat_box";
                                                textbox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                                                doc.IsHyphenationEnabled = true;
                                                textbox.Document = doc;
                                                textbox.Document.LineHeight = 1;
                                                textbox.IsDocumentEnabled = true;
                                                textbox.FontSize = Properties.Settings.Default.FontSize;
                                                rtext_boxes.Add(textbox);

                                                if (channel == "Status")
                                                {
                                                    AppendText("Succesfully connected to server.", "Status", "#22bd7f");
                                                    AppendText("Type /help to see the list of available commands.", "Status", "#4f009b");
                                                    AppendText("Join a channel with the command: /join <channel>", "Status", "#b9393f");
                                                    AppendText("For example /join LightChat", "Status", "#b9393f");

                                                    var startup_channels = Properties.Settings.Default.Startup_channel_list.Split(',');
                                                    if(startup_channels.Length > 0)
                                                    {
                                                        for (int i = 0; i < startup_channels.Length; i++)
                                                        {
                                                            if (!string.IsNullOrWhiteSpace(startup_channels[i])){
                                                                SendJoinChannel(startup_channels[i]);
                                                            }
                                                        }
                                                    }
                                                }

                                                listbox = new ListBox();
                                                listbox.HorizontalAlignment = HorizontalAlignment.Right;
                                                listbox.Width = 245;
                                                listbox.Margin = new Thickness() { Top = 25 };
                                                listbox.Name = channel + "_user_list";
                                                users_lists.Add(listbox);
                                                // add users here

                                                motd_box = new TextBox();
                                                motd_box.VerticalAlignment = VerticalAlignment.Top;
                                                motd_box.Name = channel + "_motd";
                                                motd_box.Height = 20;
                                                motd_box.KeyDown += Motd_box_KeyDown;
                                                motd_box.Text = motd_msg;
                                                motd_text_boxes.Add(motd_box);
                                                // request motd here

                                                grid = new Grid();
                                                grid.Name = channel + "_grid";
                                                grid.Children.Add(textbox);
                                                grid.Children.Add(listbox);
                                                grid.Children.Add(motd_box);

                                                item = new TabItem();
                                                item.Name = channel + "_tabitem";
                                                item.Header = channel;
                                                item.Content = grid;

                                                CMenuItem = new MenuItem();
                                                CMenuItem.Name = channel + "_context_menu";
                                                CMenuItem.Header = "Close " + channel;
                                                CMenuItem.Click += CMenuitem_Click;

                                                cmenu = new ContextMenu();
                                                cmenu.Items.Add(CMenuItem);
                                                item.ContextMenu = cmenu;
                                                tab_channels.Add(item);

                                                tab_control.Items.Add(item);
                                                tab_control.SelectedItem = item;

                                                if(channel != "Status")
                                                {
                                                    NetOutgoingMessage outmsg = client.CreateMessage();
                                                    outmsg.Write((byte)NetMessageType.RequestChannelUsers);
                                                    outmsg.Write(channel);
                                                    client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
                                                    client.FlushSendQueue();

                                                    NetOutgoingMessage outmsg2 = client.CreateMessage();
                                                    outmsg2.Write((byte)NetMessageType.RequestChannelMOTD);
                                                    outmsg2.Write(channel);
                                                    client.SendMessage(outmsg2, NetDeliveryMethod.ReliableOrdered);
                                                    client.FlushSendQueue();
                                                }                                                
                                            }
                                            catch (Exception ex)
                                            {
                                                MessageBox.Show("Error creating the Channel tab! " + ex.Message);
                                            }
                                            finally
                                            {
                                                textbox = null;
                                                grid = null;
                                                item = null;
                                                motd_box = null;
                                                doc = null;
                                            }
                                        }
                                        else
                                        {
                                            AppendText("Couldn't join channel", selected_channel, "#f45942");
                                        }
                                        break;
                                    }
                                case NetMessageType.ConfirmChannelLeave:
                                    {
                                        var leave = msg.ReadBoolean();
                                        var channel_name = msg.ReadString();

                                        if (leave)
                                        {
                                            if(channel_name == selected_channel)
                                            {
                                                tab_control.SelectedIndex = 0;
                                                selected_channel = "Status";
                                            }
                                            var tabToDelete = tab_control.Items.OfType<TabItem>().SingleOrDefault(n => (string)n.Header == channel_name);
                                            if (tabToDelete != null)
                                                tab_control.Items.Remove(tabToDelete);

                                            
                                        }
                                        else
                                        {
                                            AppendText("Error: channel doesn't exist", selected_channel);
                                        }
                                        break;
                                    }
                                case NetMessageType.RequestChannelUsers:
                                    {
                                        var channel_name = msg.ReadString();
                                        var user_list = msg.ReadString();

                                        var user_array = user_list.Split(',');

                                        var listbox = users_lists.Find(x => x.Name == channel_name + "_user_list");
                                        listbox.Items.Clear();

                                        for (int i = 0; i < user_array.Length; i++)
                                        {
                                            ListBoxItem item = new ListBoxItem();
                                            item.Content = user_array[i];
                                            listbox.Items.Add(item);
                                        }                                        
                                        break;
                                    }
                                default:
                                    break;
                            }
                            break;
                        }
                        
                    default:
                        AppendText("Unhandled type: " + msg.MessageType, selected_channel);
                        break;
                }
                client.Recycle(msg);
            }
        }

        private void Motd_box_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                TextBox motdbox = sender as TextBox;

                NetOutgoingMessage outmsg = client.CreateMessage();
                outmsg.Write((byte)NetMessageType.SendChannelMOTD);
                outmsg.Write(selected_channel);
                outmsg.Write(motdbox.Text);
                client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
                client.FlushSendQueue();
            }            
        }

        private void CMenuitem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mnu = sender as MenuItem;

            string channel = mnu.Header.ToString().Split(' ')[1];

            SendLeaveChannel(channel);
        }

        public void AppendText(string text, string channel)
        {
            var rich_text_box = rtext_boxes.Find(x => x.Name == channel + "_chat_box");
            rich_text_box.AddText(text);
            rich_text_box.ScrollToEnd();
        }

        public void AppendText(string text, string channel, string color)
        {
            var rich_text_box = rtext_boxes.Find(x => x.Name == channel + "_chat_box");
            rich_text_box.AddText(text, color);
            rich_text_box.ScrollToEnd();
        }

        private void tab_control_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TabControl tabControl = sender as TabControl;
                TabItem itm = (TabItem)tabControl.SelectedItem;

                string channel_name = (String)itm.Header;
                selected_channel = channel_name;
            }
            catch (NullReferenceException)
            {
                // Channel has been deleted

                selected_channel = "Status";
                tab_control.SelectedIndex = 0;
            }

            tab_channels.Find(x => (String)x.Header == selected_channel).ClearValue(TabItem.BackgroundProperty);
        }

        private void Preferences_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var preferences_window = new PreferencesWindow();
            preferences_window.ShowDialog();

            foreach(var tbox in rtext_boxes)
            {
                tbox.FontSize = Properties.Settings.Default.FontSize;
            }
        }
    }
}
