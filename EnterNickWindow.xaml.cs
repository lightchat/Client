﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for EnterNickWindow.xaml
    /// </summary>
    public partial class EnterNickWindow : Window
    {
        public EnterNickWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Nick = Nick_TextBox.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void Nick_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                Properties.Settings.Default.Nick = Nick_TextBox.Text;
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Nick_TextBox.Focus();
        }
    }
}
