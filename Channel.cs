﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Channel
    {
        public string name { get; set; }
        public string content { get; set; }
        public string motd { get; set; }
        public List<User> users = new List<User>();
    }
}
