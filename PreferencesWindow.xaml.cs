﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for PreferencesWindow.xaml
    /// </summary>
    public partial class PreferencesWindow : Window
    {
        public PreferencesWindow()
        {
            InitializeComponent();
        }

        public string HashPassword(string raw_password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(raw_password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string savedPasswordHash = Convert.ToBase64String(hashBytes);

            return savedPasswordHash;
        }

        private void password_box_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Properties.Settings.Default.hash = HashPassword(password_box.Password);
                Properties.Settings.Default.Save();
            }
        }

        private void username_field_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Properties.Settings.Default.username = username_field.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(!String.IsNullOrWhiteSpace(Properties.Settings.Default.username))
            {
                username_field.Text = Properties.Settings.Default.username;
            }

            if(!String.IsNullOrWhiteSpace(Properties.Settings.Default.Startup_channel_list))
            {
                channels_startup_box.Text = Properties.Settings.Default.Startup_channel_list;
            }

            if(Properties.Settings.Default.autologin)
            {
                remember_checkbox.IsChecked = true;
            }
            fontsize_slider.Value = Properties.Settings.Default.font_slide_value;

            fontsize_size_label.Content = Math.Truncate(Properties.Settings.Default.FontSize * 10) / 10 + "px";
        }

        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.hash = HashPassword(password_box.Password);
            Properties.Settings.Default.username = username_field.Name;
            Properties.Settings.Default.Startup_channel_list = channels_startup_box.Text;
            Properties.Settings.Default.Save();
        }

        private void remember_checkbox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.autologin = true;
            Properties.Settings.Default.Save();
        }

        private void remember_checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.autologin = false;
            Properties.Settings.Default.Save();
        }

        private void fontsize_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double val = (fontsize_slider.Value * 1.5 + 14);
            fontsize_size_label.Content = Math.Truncate(val * 10) / 10 + "px";
            test_label.FontSize = val;
            Properties.Settings.Default.FontSize = val;
            Properties.Settings.Default.font_slide_value = fontsize_slider.Value;
            Properties.Settings.Default.Save();
        }
    }
}
