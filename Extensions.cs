﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Navigation;

namespace Client
{
    public static class Extensions
    {
        private static readonly Regex UrlRegex = new Regex(@"(?#Protocol)(?:(?:ht|f)tp(?:s?)\:\/\/|~/|/)?(?#Username:Password)(?:\w+:\w+@)?(?#Subdomains)(?:(?:[-\w]+\.)+(?#TopLevel Domains)(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel|[a-z]{2}))(?#Port)(?::[\d]{1,5})?(?#Directories)(?:(?:(?:/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|/)+|\?|#)?(?#Query)(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&amp;(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?#Anchor)(?:#(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)?");

        public static void AddText(this RichTextBox textbox, string text, string color = null)
        {
            Paragraph lol = new Paragraph();
            TextPointer start = lol.ContentStart;

            int i = 0;
            foreach (var word in text.Split(' ').ToList())
            {
                if (UrlRegex.IsMatch(word))
                {
                    Hyperlink link = new Hyperlink();
                    link.IsEnabled = true;
                    link.Inlines.Add(word);
                    link.NavigateUri = new Uri(word);
                    link.RequestNavigate += new RequestNavigateEventHandler(link_RequestNavigate);
                    link.Style = (Style)Application.Current.Resources["linkstyle1"];
                    link.Foreground = Brushes.Black;

                    lol.Inlines.Add(link);
                    lol.Inlines.Add(" ");
                }
                else
                {
                    lol.Inlines.Add(word + " ");
                }

                i++;
            }

            if (!String.IsNullOrWhiteSpace(color))
            {
                BrushConverter bc = new BrushConverter();
                TextRange tr = new TextRange(lol.ContentStart, lol.ContentEnd);
                try
                {
                    tr.ApplyPropertyValue(TextElement.ForegroundProperty,
                        bc.ConvertFromString(color));
                }
                catch (FormatException) { }
            }

            lol.LineHeight = 1;
            textbox.Document.Blocks.Add(lol);
        }

        private static void link_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri.ToString());
        }

        public static void AppendText(this RichTextBox box, string text, string color)
        {
            BrushConverter bc = new BrushConverter();
            TextRange tr = new TextRange(box.Document.ContentEnd, box.Document.ContentEnd);
            tr.Text = text;
            try
            {
                tr.ApplyPropertyValue(TextElement.ForegroundProperty,
                    bc.ConvertFromString(color));
            }
            catch (FormatException) { }
        }
    }
}
