# LightChat Client

Currently in a very early stage of development.

LightChat is a chat client that currently have:
- Channel support
- Commands

Still to be done:
- Private Channels
- Direct Messages
- Register / login system (optional)
- Finish the User Interface
- Channel Operators (Moderation system)
- Bot API
- Friend list
- Encryption